/*******************************************************************************
** 文件名: 		comm.c
** 版本：  		1.0
** 工作环境: 	RealView MDK-ARM 4.20
** 作者: 		吴国炎
** 生成日期: 	2011-04-10
** 功能:		模版程序（用户可以在这里简单说明工程的功能）
** 相关文件:	无
** 修改日志：	2011-04-10   创建文档
*******************************************************************************/
/* 包含头文件 *****************************************************************/
#include "main.h"
#include "stm32f0xx_hal.h"
#include "cmsis_os.h"

#include "string.h"

#include "CMD_Receive.h"
#include "datatype.h"
#include "fifo.h"
#include "main.h"
#include "comm_module.h"
#include "flow_module.h"
#include "sampling_module.h"
/* 类型声明 ------------------------------------------------------------------*/
/* 宏定义 --------------------------------------------------------------------*/

/* 变量 ----------------------------------------------------------------------*/
static CMD_Receive _com2_CmdReceive;  // 命令接收控制对象

FIFO_TYPE com2_tx_fifo;
extern FLOW_MODULE flow_module;
static UINT8 _com2_fifo_buffer[TX_FIFO_LEN];

extern uint32_t PWM_DC_MAX;
/* 函数声明 ------------------------------------------------------------------*/

/* 函数功能 ------------------------------------------------------------------*/
/******************************************************
*内部函数定义
******************************************************/

//--------------------------公共部分-----------------------------//
static BOOL ModuleUnPackFrame(void);
static BOOL ModuleProcessPacket(UINT8 *pData);
static UINT8 CheckCheckSum(UINT8* pData, UINT8 nLen);

extern SAMPLING_MODULE sampling_module;

//计算存储数据校验和
UINT32 _get_check_sum(uint8_t* buf, uint16_t len)
{
	UINT32 check_sum = 0;
	UINT16 cnt;
	
	for(cnt = 0; cnt < len; cnt ++)
	{
		check_sum += buf[cnt];
	}
	
	return check_sum;
}

void CalcCheckSum(UINT8* pPacket)
{
	UINT16 dataLen = pPacket[1];
	UINT16 checkSum = 0;
	UINT16 i;

	for (i = 1; i < dataLen; i++)
	{
		checkSum += pPacket[i];
	}

	pPacket[dataLen] = checkSum >> 8;
	pPacket[dataLen+1] = checkSum&0xFF;
}

/*******************************************************************************
** 函数名称: COM2_ModuleUnPackFrame
** 功能描述: 命令接收处理
** 输　  入: 无
** 输　  出: 无
** 全局变量: 无
** 调用模块: 无
*******************************************************************************/
BOOL COM2_ModuleUnPackFrame(void)
{
	static BOOL sPacketHeadFlag = FALSE;
	static BOOL sPacketLenFlag = FALSE;
	static UINT8 sCurPacketLen = 0;
	static UINT8 sResetByte = 0;
	static UINT8 sDataBuff[CMD_BUFFER_LENGTH] = {0};	
	static UINT8 sBackBuff[CMD_BUFFER_LENGTH] = {0};

	UINT8 *pBuff = (UINT8 *)sDataBuff;
	UINT16 dwLen = 0;
	UINT8 byCurChar;

	// 从串口缓冲区中读取接收到的数据
	dwLen = GetBuf2Length(&_com2_CmdReceive);

	// 对数据进行解析
	while(0 < dwLen)
	{
		byCurChar = Buf2Read(&_com2_CmdReceive);

		if (sPacketHeadFlag)
		{
			// 解析到包头
			if(sPacketLenFlag)
			{
				// 解析到包长度
				pBuff[sCurPacketLen] = byCurChar;
				sCurPacketLen ++;
				sResetByte --;

				if (0 >= sResetByte)
				{
					// 接收完毕
					// 进行校验和比较
					if (CheckCheckSum(pBuff, pBuff[1]))
					{
						// 解析到一个有效数据包
						memcpy(sBackBuff, sDataBuff, CMD_BUFFER_LENGTH);
						ModuleProcessPacket(sBackBuff);
						//防止连续接收多条命令时，命令应答信号出故障
						vTaskDelay(2/portTICK_RATE_MS );
					}

					sPacketHeadFlag = FALSE;
					sPacketLenFlag = FALSE;
					memset(&sDataBuff, 0x00, CMD_BUFFER_LENGTH);
				}													
			}
			else
			{
				if((CMD_BUFFER_LENGTH-1 > byCurChar) && (0 < byCurChar ))// 容错处理，防止数据包长为49或0时溢出 并且X5最长的主机发送包为15
				{
					// 解析到模块的长度
					sDataBuff[sCurPacketLen] = byCurChar;
					sResetByte = byCurChar;			
					sPacketLenFlag = TRUE;
					sCurPacketLen ++;
				}
				else
				{
					//没有解析到模块的长度, 重新解析
					sPacketHeadFlag = FALSE;
					sPacketLenFlag = FALSE;					
				}
			}
		}
		else if (PACK_HEAD_BYTE == byCurChar)		
		{
			// 解析到包头
			sDataBuff[0] = byCurChar;
			sPacketHeadFlag = TRUE;
			sPacketLenFlag = FALSE;			
			sCurPacketLen = 1;
			sResetByte = 0;
		}

		//pData ++;
		dwLen --;
	}
	return TRUE;
}
/*******************************************************************************
** 函数名称: CheckCheckSum
** 功能描述: 包校验
** 输　  入: pData 数据 nLen长度
** 输　  出: 无
** 全局变量: 无
** 调用模块: 无
*******************************************************************************/
UINT8 CheckCheckSum(UINT8* pData, UINT8 nLen)
{
	UINT16 bySum = 0;
	int i;
	// 计算数据的校验和	
	for(i = 1; i < nLen; i++)
	{
		bySum += pData[i];
	}		

	if (bySum == (pData[nLen] << 8)+ pData[nLen + 1])
	{
		return TRUE;
	}
	else
	{
		return FALSE;	
	}
}

//--------------------------发送部分-----------------------------//
//发送实时数据
//void send_fio2_flow_data(u16 fio2, u16 flow_lpm, u16 flow_slpm)
void send_fio2_flow_data()
{
	u8 i;
	u8 buffer[40];      //组成：头(4 Bytes)+数据(17 Bytes)+保留字(17 Bytes)+checksum(2 Bytes)=40
	u8 *pfio2, *pflow_lpm, *pflow_slpm;
	u32t pwm_percent=0;
	
	//TODO,发送数据待修改
	buffer[0] = PACK_HEAD_BYTE;
	buffer[1] = 40-2;
	buffer[2] = MODULE_CMD_TYPE;     //0x00
	buffer[3] = SLAVER_SEND_DATA;    //0x92
	
	//flow_module中只发送flow_slpm，flow_lpm_set，flow_slpm
	buffer[4] = (u8)((flow_module.flow_slpm >> 8) & 0xff);      //flow_slpm
	buffer[5] = (u8)flow_module.flow_slpm;
	
	buffer[6] = (u8)((flow_module.flow_lpm_set >> 8) & 0xff);   //flow_lpm_set
	buffer[7] = (u8)flow_module.flow_lpm_set;
	
	#ifdef DEBUG_CHANGE_FREQUENCE
	pwm_percent=100*flow_module.pro_pwm/PWM_DC_MAX;
	buffer[8] = (u8)((pwm_percent >> 8) & 0xff);        //pro_pwm
	buffer[9] = (u8)pwm_percent;
	#else
	buffer[8] = (u8)((flow_module.pro_pwm >> 8) & 0xff);        //pro_pwm
	buffer[9] = (u8)flow_module.pro_pwm;
	#endif

	//sampling_module中发送
	buffer[10] = (u8)((sampling_module.avg_flow >> 8) & 0xff); 	//avg_flow
	buffer[11] = (u8)sampling_module.avg_flow;
	
	buffer[12] = (u8)((sampling_module.flow >> 8) & 0xff);    	//flow
	buffer[13] = (u8)sampling_module.flow;                   
	
	buffer[14] = (u8)sampling_module.sensor_type;           		//sensor_type
	buffer[15]=(u8)sampling_module.sensor_err;             			//sensor_err
	
	buffer[16]=(u8)((sampling_module.cur_adc>>8)&0xff);   		  //cur_adc
	buffer[17]=(u8)sampling_module.cur_adc;
	
	buffer[18]=(u8)sampling_module.IS_valve_exist;              //Is valve exist
	buffer[19]=(u8)sampling_module.IS_5V_ok;                    //Is 5V ok
	buffer[20]=(u8)sampling_module.IS_12V_ok;                   //Is 12V ok
	 
	//保留字
	for(int i=0;i<17;i++)
	{
		buffer[21+i]=0;
	}
	
	CalcCheckSum(buffer);
	
	fifoWriteData(&com2_tx_fifo, buffer, buffer[1]+2);
}

void send_modify_PID_result(u8 result)
{
	u8 buffer[7];    
	u16 tx_len;
	u8 com2_txbuffer[TX_FIFO_LEN];	
	
	buffer[0] = PACK_HEAD_BYTE;
	buffer[1] = 7-2;
	buffer[2] = MODULE_CMD_TYPE;     //0x00
	buffer[3] = SLAVER_SEND_MODIFY_PID_SUCCESS;    //0xAB
	

	buffer[4] = result;      //result=0，表示disable成功(还原了PID);result=1,表示修改PID成功
	 
	CalcCheckSum(buffer);
	
	fifoWriteData(&com2_tx_fifo, buffer, buffer[1]+2);
//	vTaskDelay(20 /portTICK_RATE_MS);
//	
//	tx_len = fifoReadData(&com2_tx_fifo, com2_txbuffer, TX_FIFO_LEN);  //将com2_tx_fifo读到com2_txbuffer中
//		if(tx_len > 0)
//			interface_module_transmit(COM2, com2_txbuffer,tx_len);      //使用DMA发送数据
}


//--------------------------接收处理-----------------------------//
/*******************************************************************************
** 函数名称: ModuleProcessPacket
** 功能描述: 命令解包
** 输　  入: pData 命令
** 输　  出: 无
** 全局变量: 无
** 调用模块: 无
*******************************************************************************/
BOOL ModuleProcessPacket(UINT8 *pData)
{
	UINT8 *pCmdPacketData = (UINT8 *)pData;
	UINT8 byFrameID = pCmdPacketData[3];
	UINT16 utemp = 0;
	INT32 slope;
	INT32 intercept;
	INT16 _press;

	switch(byFrameID)
	{
		case HOST_SEND_TEST_ID:
			break;
		case HOST_SEND_SETTING_FLOW:
			set_flow_parameter(pData);
			break;
		case HOST_SEND_MODIFY_PID:
			set_pid(pData);
			break;
		case HOSET_SEND_GET_PID:
			send_pid_2_PC(pData);
			break;
		case HOST_SEND_MANUAL_SET_PWM_DC:
			set_pwm_dc(pData,HOST_SEND_MANUAL_SET_PWM_DC);
			break;
		case HOST_SEND_TEST_VALVE_CURVE:
			set_pwm_dc(pData,HOST_SEND_TEST_VALVE_CURVE);
			break;
		default:
			break;
	}
	
	printf("com2 rev ok!\n");
	
	return TRUE;     	
}

//初始化
void comm_module_init(void)
{
	fifoInit(&com2_tx_fifo,_com2_fifo_buffer,TX_FIFO_LEN);
	
	interface_module_receive(COM2, _com2_CmdReceive.m_Buf1, BUF1_LENGTH);
}

//传播间隔计算任务
void comm_task(void* pvParamemters)
{
	u16 re_delay = 0;
	u16 tx_len;
	u8 com2_txbuffer[TX_FIFO_LEN];
	static u32t sum_flow=0;
	
	comm_module_init();
	vTaskDelay(10/portTICK_RATE_MS );
	
	while(1)
	{
		#if 0
//		//-------------------接收数据并处理-------------------------//
//		re_delay++;
//		if(re_delay > 50) // 20*50=1000ms的频率解包
//		{
//			re_delay = 0;
//			ReceiveData(&_com2_CmdReceive,COM2);
//			COM2_ModuleUnPackFrame();
//			
//			//读SPI FLASH ID
////			send_fio2_flow_data(0,flow_module.flow_lpm_rt,flow_module.flow_slpm);
//			send_fio2_flow_data();   //将数据写入com2_tx_fifo中，通过interface_module_transmit发送
//			
//		}
//		
//		
//		//-------------------发送数据-------------------------------//
//		tx_len = fifoReadData(&com2_tx_fifo, com2_txbuffer, TX_FIFO_LEN);  //将com2_tx_fifo读到com2_txbuffer中
//		if(tx_len > 0)
//			interface_module_transmit(COM2, com2_txbuffer,tx_len);
		#endif
		
		if(re_delay>=5)
		{
			re_delay=0;
			
			sampling_module.avg_flow=sum_flow/5;   //flow的平均值
			sum_flow=0;
			
			send_fio2_flow_data();                 //将数据写到fifo中的com2_tx_fifo
//			send_modify_PID_result(1);
			
			tx_len = fifoReadData(&com2_tx_fifo, com2_txbuffer, TX_FIFO_LEN);  //将com2_tx_fifo读到com2_txbuffer中
			if(tx_len > 0)
				interface_module_transmit(COM2, com2_txbuffer,tx_len);      //使用DMA发送数据
		}
		else           
		{
			
			ReceiveData(&_com2_CmdReceive,COM2);  //接收数据
			COM2_ModuleUnPackFrame();             //解包，获取上位机的命令，设置start_flow,end_flow等参数
			
			sum_flow+=sampling_module.flow;

			re_delay++;
		}
		

		vTaskDelay(20/portTICK_RATE_MS );
	}
}
