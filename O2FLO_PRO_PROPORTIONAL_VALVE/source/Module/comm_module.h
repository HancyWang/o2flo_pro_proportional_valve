/****************************************************
*版权所有：深圳XXX有限公司
*文件名：comm_module.h
*功能说明：
*作者：
*生成日期：
*****************************************************/
#ifndef  __COMM_MODULE_H_
#define  __COMM_MODULE_H_

/*****************************************************
*头文件包含
******************************************************/
#include "datatype.h"
#include "fifo.h"
#include "common.h"
/******************************************************
*内部宏定义
******************************************************/
#define   CMD_BUFFER_LENGTH         	40  //宏定义命令数据包的长度
#define	  PACK_HEAD_BYTE				0xFF //头文件标志
#define   MODULE_CMD_TYPE				0


#define  TX_FIFO_LEN  40

//--------------------模块发送----------------//
#define MODULE_SEND_FIO2_FLOW_RT_ID   	0x01 //50HZ
#define MODULE_SEND_FIO2_FLOW_PRESS_TEMP_AVG_ID   	0x02 //1HZ
#define MODULE_SEND_CALI_PATA_ID  0x10 //1HZ 作为校准用

//调试
#define MODULE_SEND_FIO2_TIME_ID  0x20
#define MODULE_SEND_TEST_ID  0x80
//--------------------主机发送----------------//
#define HOST_SEND_TEST_ID  0x80
#define HOST_SEND_SETTING_FLOW 0x91
#define SLAVER_SEND_DATA			 0x92

#define HOST_SEND_MODIFY_PID    						 0xAA
#define SLAVER_SEND_MODIFY_PID_SUCCESS 			 0xAB
#define HOSET_SEND_GET_PID									 0xAC
#define SLAVER_SEND_PID_GET_RESULT					 0xAD      
#define HOST_SEND_MANUAL_SET_PWM_DC					 0xAE      //手动设置比例阀PWM的D.C
#define HOST_SEND_TEST_VALVE_CURVE				   0xAF      //测试比例阀的特性曲线
/******************************************************
*内部类型定义
******************************************************/


/******************************************************
*外部变量声明
******************************************************/

/******************************************************
*外部函数声明
******************************************************/
void comm_task(void* pvParamemters);
#endif
