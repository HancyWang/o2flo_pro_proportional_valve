/*******************************************************************************
** 文件名: 		sampling_module.c
** 版本：  		1.0
** 工作环境: 	RealView MDK-ARM 4.20
** 作者: 		吴国炎
** 生成日期: 	2011-04-10
** 功能:		模版程序（用户可以在这里简单说明工程的功能）
** 相关文件:	无
** 修改日志：	2011-04-10   创建文档
*******************************************************************************/
/* 包含头文件 *****************************************************************/
#include "main.h"
#include "stm32f0xx_hal.h"
#include "cmsis_os.h"
#include "datatype.h"
#include "fifo.h"
#include "main.h"
#include "sampling_module.h"
#include "interface_module.h"
#include "comm_module.h"
#include "fifo.h"
#include "common.h"
#include "bsp_iic2_flow.h"
#include "sf05.h"

/* 类型声明 ------------------------------------------------------------------*/

/* 宏定义 --------------------------------------------------------------------*/

/* 变量 ----------------------------------------------------------------------*/
extern I2C_HandleTypeDef hi2c1;
extern UART_HandleTypeDef huart2;
extern ADC_HandleTypeDef hadc;
//extern DMA_HandleTypeDef hdma_adc;
extern uint16_t ADC_Value[3];

//采样模块
SAMPLING_MODULE sampling_module;
FIFO_TYPE comm_fifo;
uint16_t adc_data_buf[10] = {0};

/* 函数声明 ------------------------------------------------------------------*/
void sampling_module_detect_flow(SAMPLING_MODULE* module); 
BOOL _get_memsic_flow(SAMPLING_MODULE* module,u8 channel, u16 *flow, u16 sampling_cnt);
BOOL _get_senirion_flow(SAMPLING_MODULE* module,u8 channel,u16 offset, u16 scale, u16 *flow, u16 sampling_cnt);

/* 函数功能 ------------------------------------------------------------------*/
/******************************************************
*内部函数定义
******************************************************/
void sampling_module_init(SAMPLING_MODULE * module)
{
	module->flow = 0;
	module->sensor_err = NO_ERROR;
	module->sensor_type = SENIRION;
	
	
	//fifoInit(&curr_fifo, (u8*)_curr_fifo_buffer, CURR_FIFO_BUFFER_LEN<<1);
}
//打开MEMSIC流量传感器电源
void sampling_module_open_flow_sensor_power(u8 flg)
{
	if(flg)
	{
		HAL_GPIO_WritePin(VFLOW_EN_GPIO_Port,VFLOW_EN_Pin,GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(VFLOW_EN_GPIO_Port,VFLOW_EN_Pin,GPIO_PIN_RESET);
	}
}

/********************************************************************************
*流量采样部件
*********************************************************************************/
//SENIRION
BOOL _get_senirion_flow(SAMPLING_MODULE* module,u8 channel,u16 offset, u16 scale, u16 *flow, u16 sampling_cnt)
{
	u8 cnt;
	u16 senirion_sampling_cnt = sampling_cnt;
	static u16 flow_err_cnt = 0;
	if(NO_ERROR == SF05_GetFlow(channel, offset, scale, flow, SENIRION))
	{
		flow_err_cnt = 0;
		if(channel == EEPROM_CH)
		module->sensor_err &= ~OPT_SENSOR_ERR;
		else if(channel == FLOW_CH)
			module->sensor_err &= ~OXY_SENSOR_ERR;
		else
			module->sensor_err &= ~AIR_SENSOR_ERR;
		
		return TRUE;
	}
	else
	{
		flow_err_cnt++;
		if(flow_err_cnt > 200)
		{
			flow_err_cnt = 1000;
			if(channel == EEPROM_CH)
				module->sensor_err |= OPT_SENSOR_ERR;
			else if(channel == FLOW_CH)
				module->sensor_err |= OXY_SENSOR_ERR;
			else
				module->sensor_err |= AIR_SENSOR_ERR;
			
			//错误
			return FALSE;
		}
	}
	return FALSE;
	
#if 0	
	for(cnt = 0; cnt < senirion_sampling_cnt; cnt ++)
	{
		//得到opt采样值
		if(NO_ERROR == SF05_GetFlow(channel, offset, scale, flow, SENIRION) && flow_err_cnt < 100)
		{			
			//OK
			flow_err_cnt = 0;
			break;
		}
		
		//延时10ms
		OSTimeDlyHMSM(0,0,0,10);
	}

	if(cnt >= senirion_sampling_cnt)
	{
		flow_err_cnt++;
		if(flow_err_cnt > 15)
		{
			flow_err_cnt = 100;
			//错误
			if(channel == EEPROM_CH)
				module->sensor_err |= OPT_SENSOR_ERR;
			else if(channel == FLOW_CH)
				module->sensor_err |= OXY_SENSOR_ERR;
			else
				module->sensor_err |= AIR_SENSOR_ERR;
			
			//错误
			return FALSE;
		}
	}
	
	if(channel == EEPROM_CH)
		module->sensor_err &= ~OPT_SENSOR_ERR;
	else if(channel == FLOW_CH)
		module->sensor_err &= ~OXY_SENSOR_ERR;
	else
		module->sensor_err &= ~AIR_SENSOR_ERR;
	
	
	return TRUE;
	
#endif
}
//MEMSIC
BOOL _get_memsic_flow(SAMPLING_MODULE* module,u8 channel, u16 *flow, u16 sampling_cnt)
{
	u8 cnt;
	u16 memsic_sampling_cnt = sampling_cnt;
	static u16 flow_err_cnt = 0;
	#if 0
	//得到opt采样值
		if(NO_ERROR == SF05_GetFlow(channel, 0, 0, flow, MEMSIC))
		{
			flow_err_cnt = 0;
			if(channel == EEPROM_CH)
			module->sensor_err &= ~OPT_SENSOR_ERR;
			else if(channel == FLOW_CH)
				module->sensor_err &= ~OXY_SENSOR_ERR;
			else
				module->sensor_err &= ~AIR_SENSOR_ERR;
			
			return TRUE;
		}
		else
		{
			flow_err_cnt++;
			if(flow_err_cnt > 200)
			{
				flow_err_cnt = 1000;
				//错误
				if(channel == EEPROM_CH)
					module->sensor_err |= OPT_SENSOR_ERR;
				else if(channel == FLOW_CH)
					module->sensor_err |= OXY_SENSOR_ERR;
				else
					module->sensor_err |= AIR_SENSOR_ERR;

				return FALSE;
			}
		}
#endif

#if 1	
	for(cnt = 0; cnt < memsic_sampling_cnt; cnt ++)
	{
		
		//得到opt采样值
		if(NO_ERROR == SF05_GetFlow(channel, 0, 0, flow, MEMSIC))
		{			
			//OK
			break;
		}
		
		//读指令
		SF05_SetMeasurement(channel,MEMSIC);
		
		//延时10ms
		vTaskDelay(10/portTICK_RATE_MS ); 
	}
	
	if(cnt >= memsic_sampling_cnt)
	{
		//错误
		if(channel == EEPROM_CH)
			module->sensor_err |= OPT_SENSOR_ERR;
		else if(channel == FLOW_CH)
			module->sensor_err |= OXY_SENSOR_ERR;
		else
			module->sensor_err |= AIR_SENSOR_ERR;

		return FALSE;
	}
	
		//OK
	if(channel == EEPROM_CH)
		module->sensor_err &= ~OPT_SENSOR_ERR;
	else if(channel == FLOW_CH)
		module->sensor_err &= ~OXY_SENSOR_ERR;
	else
		module->sensor_err &= ~AIR_SENSOR_ERR;
	
	return TRUE;
	
#endif
}

void sampling_module_detect_flow(SAMPLING_MODULE* module)  
{
	u32 flow_sum = 0;
	u16 flow = 0;
	u8 cnt;
	static u16 pre_flow = 0;
	static u16 flow_cnt = 0;
	static u16 flow_zero_cnt = 0;
	//发送测量命令
	if(module->sensor_type == MEMSIC)
		SF05_SetMeasurement(FLOW_CH,module->sensor_type);
	//延时
	vTaskDelay(10 /portTICK_RATE_MS);

	////得到流量
	if(module->sensor_type == MEMSIC)
		_get_memsic_flow(module,FLOW_CH, &flow,50);
	else
		_get_senirion_flow(module,FLOW_CH,OFFSET_FLOW, SCALE_FLOW_O2, &flow, 25);
/* 在设备工作时，关闭传感器电源并软件复位，解决传感器在过EFT时，传感器死机现象*/
		if(module->sensor_type == SENIRION)
		{
			if(pre_flow == flow)
			{
				flow_cnt++;
				if(flow_cnt > 100)
				{
					flow_cnt = 0;
					sampling_module_open_flow_sensor_power(FALSE);		//关闭5V电源
					IIC1_SCL_L;// = 0;
					IIC1_SDA_L;// = 0;
					vTaskDelay(100/portTICK_RATE_MS ); 
					sampling_module_open_flow_sensor_power(TRUE);			//打开5V电源
					IIC1_SCL_H;// = 1;
					IIC1_SDA_H;// = 1;
					vTaskDelay(10/portTICK_RATE_MS ); 
					SF05_SoftReset(FLOW_CH,SENIRION);		//软件复位
				}
			}
			pre_flow = flow;
		}
		if(flow == 0)
		{
			flow_zero_cnt++;
			if(flow_zero_cnt < 200)
			{
				return;
			}
		}
	flow_zero_cnt = 0;
/* end  */	
	
	//得到平均流量
	module->flow = flow;
}


void fill_up_sampling_module(SAMPLING_MODULE *module)
{
	uint16_t adc_5V=0;
	uint16_t adc_12V=0;
	module->cur_adc=ADC_Value[0];  //比例阀电流
	adc_12V=ADC_Value[1];
	adc_5V=ADC_Value[2];
	
	if(adc_5V>=500&&adc_5V<=625)
	{
		module->IS_5V_ok=1;
	}
	else
	{
		module->IS_5V_ok=0;
	}
	
	if(adc_12V>=1700&&adc_12V<=2000)
	{
		module->IS_12V_ok=1;
	}
	else
	{
		module->IS_12V_ok=0;
	}
}

////得到比例阀电流
//void sampling_module_get_cur_adc(SAMPLING_MODULE *module)
//{
//	
//    
////	HAL_ADC_Start(&hadc);
////	HAL_ADC_PollForConversion(&hadc,500);
////	module->cur_adc = HAL_ADC_GetValue(&hadc);
//	module->cur_adc=ADC_Value[0];
//	
//  
//}

//void sampling_module_is_5V_ok(SAMPLING_MODULE* module)
//{
//	static UINT32 adc_5v=0;
//	HAL_ADC_Start(&hadc_5V);
//	HAL_ADC_PollForConversion(&hadc_5V,500);
//	adc_5v = HAL_ADC_GetValue(&hadc_5V);
//}

//void sampling_module_is_12V_ok(SAMPLING_MODULE* module)
//{
//	static UINT32 adc_12v=0;
//	HAL_ADC_Start(&hadc_12V);
//	HAL_ADC_PollForConversion(&hadc_12V,500);
//	adc_12v = HAL_ADC_GetValue(&hadc_12V);
//}

//采样任务
void sampling_task(void* pvParamemters)
{
	//任务初始化
	sampling_module_init(&sampling_module);
	
//	HAL_ADC_Start_DMA(&hadc,(uint32_t *)&adc_data_buf,sizeof(adc_data_buf));
	
	while(1)
	{    	
//		sampling_module_get_cur_adc(&sampling_module);	//得到比例阀电流

		sampling_module_detect_flow(&sampling_module);	//得到流量

		fill_up_sampling_module(&sampling_module);
		
		vTaskDelay(10/portTICK_RATE_MS ); 
	}		
}
